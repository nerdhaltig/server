{ config, pkgs }:
{
  services.roundcube = {
    enable = true;
    # this is the url of the vhost, not necessarily the same as the fqdn of
    # the mailserver
    hostName = "webmail.nerdhaltig.de";
    extraConfig = ''
      # starttls needed for authentication, so the fqdn required to match
      # the certificate
      $config['smtp_server'] = "tls://${config.mailserver.fqdn}";
      $config['smtp_port'] = 465;
      $config['smtp_user'] = "%u";
      $config['smtp_pass'] = "%p";
    '';
  };

  services.nginx.virtualHosts = {
    "webmail.nerdhaltig.de" = {
      enableACME = false;
      forceSSL = false;
      #sslCertificate = ./.;
      #sslCertificateKey = "/var/host.key";
      listen = [{
        addr = "127.0.0.1";
        port = 5151;
      }];
      #listen = 5151;
    };
  };

  services.traefik = {

    dynamicConfigOptions = {
      http = {
        routers = {
          webmailer = {
            entrypoints = "https";
            tls = { certresolver = "letsencrypt"; };
            rule = "Host(`webmail.nerdhaltig.de`)";
            service = "webmailer";
          };
        };

        services = {
          webmailer = {
            loadBalancer = {
              servers =
                [{ url = "http://127.0.0.1:5151"; }];
            };
          };
        };

      };
    };
  };

}
