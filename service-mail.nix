#
# RSPAMD
#
# ssh -L 3333:/run/rspamd/worker-controller.sock nerdhaltig.de
# http://localhost:3333

{ config, pkgs }:
let
  release = "nixos-22.05";
  credentials = import ./credentials.nix;
in
{
  imports = [
    (builtins.fetchTarball {
      url = "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/archive/${release}/nixos-mailserver-${release}.tar.gz";
      # This hash needs to be updated
      sha256 = "sha256:0f4d4slwjxxyplrhqy4prsw31n022mddkqa0gag5ii36gzcla0c6";
    })
  ];

  mailserver = {
    enable = true;
    fqdn = "nerdhaltig.de";
    domains = [ "nerdhaltig.de" "evilbrain.de" ];
    enablePop3Ssl = false;
    enableImapSsl = false;
    #enableSubmission = false;
    enableSubmissionSsl = false;
    loginAccounts = credentials.mailaccounts;
  };

  # this is somehow disabled
  services.postfix.config.smtpd_tls_auth_only = pkgs.lib.mkForce false;



  services.traefik = {
    enable = true;

    # static config
    staticConfigOptions = {
      entryPoints = {
        smtps = { address = ":465"; };
        imaps = { address = ":993"; };
      };
    };

    dynamicConfigOptions = {
      tcp = {
        routers = {
          smtps = {
            entrypoints = "smtps";
            tls = { certresolver = "letsencrypt"; };
            rule = "HostSNI(`nerdhaltig.de`) || HostSNI(`evilbrain.de`) || HostSNI(`mail.nerdhaltig.de`) || HostSNI(`mail.evilbrain.de`)";
            service = "smtp";
          };
          imaps = {
            entrypoints = "imaps";
            tls = { certresolver = "letsencrypt"; };
            rule = "HostSNI(`nerdhaltig.de`) || HostSNI(`evilbrain.de`) || HostSNI(`mail.nerdhaltig.de`) || HostSNI(`mail.evilbrain.de`)";
            service = "imap";
          };
        };
        services = {
          smtp = {
            loadBalancer = { servers = [{ address = "127.0.0.1:25"; }]; };
          };
          imap = {
            loadBalancer = { servers = [{ address = "127.0.0.1:143"; }]; };
          };
        };
      };
    };
  };



  networking.firewall.allowedTCPPorts = [ 25 465 993 ];

}
