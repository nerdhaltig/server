{ config, pkgs }:
let

in
{

  services.influxdb2 = {
    enable = true;

  };

  systemd.services.influxdb2.serviceConfig.CPUQuota = "100%";



  services.traefik = {

    dynamicConfigOptions = {
      http = {
        routers = {
          influx = {
            entrypoints = "https";
            tls = { certresolver = "letsencrypt"; };
            rule = "Host(`influx.nerdhaltig.de`)";
            service = "influx";
          };
        };

        services = {
          influx = {
            loadBalancer = {
              servers =
                [{ url = "http://127.0.0.1:8086"; }];
            };
          };
        };

      };
    };
  };

}
