{ config, pkgs }: {
  services.openssh.enable = true;

  services.openssh.listenAddresses = [{
    addr = "0.0.0.0";
    port = 2222;
  }];
  services.openssh.openFirewall = true;
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGswARuaiKJkUiuxsG5SHHL6Y5FZ8fHNdI6VFVpnMdx1 stackshadow@hacktop10"
  ];

  services.fail2ban = {
    jails.sshd-nerdhaltig = ''
      filter = sshd
      maxretry = 3
      action = iptables[name=sshdnerdhaltig, port=2222, protoco=tcp]
      enabled = true
    '';
  };

  networking.firewall.allowedTCPPorts = [ 2222 ];
}
