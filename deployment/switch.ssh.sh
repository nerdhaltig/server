#!/bin/sh
set -e

# start ssh-agent to authenticate
eval $(ssh-agent -s)

if [ "$sshkey" != "" ]; then
    echo "$sshkey" | tr -d '\r' | base64 -d | tr -d '\r' | ssh-add -
else
    echo "WARN: sshkey not set, we use the host one"
fi

if [ "$sshhostkey" != "" ]; then
    mkdir -p ~/.ssh
    echo "$sshhostkey" > ~/.ssh/known_hosts
fi

# git crypt
if [ "$GPG_PRIVATE_KEY" != "" ]; then
    gpg-agent --daemon || true
    echo "$GPG_PRIVATE_KEY" | base64 -d > ./.private.key
    gpg --batch --import ./.private.key
    git secret reveal -f
    rm ./.private.key
else
    echo "WARN: No GPG_PRIVATE_KEY private key set, we use the host one"
fi

rsync -av --delete \
-e "ssh -p 2222" \
./*.nix \
root@nerdhaltig.de:/etc/nixos/

SSH="ssh -p 2222 root@nerdhaltig.de"
${SSH} "nix-channel --add https://gitlab.com/nerdhaltig/homepage/-/archive/main/homepage-main.tar.bz2 nerdhaltig"
${SSH} "nix-channel --update nerdhaltig"

if [ "$ACTION" == "UPDATE" ]; then
    ${SSH} "nix-channel --update"
fi

${SSH} "nixos-rebuild switch"
${SSH} "rm -vfR /etc/nixos/*"
