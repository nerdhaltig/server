#!/bin/sh
set -e

nix-env -iA nixos.gnupg nixos.gnused nixos.git-secret

# git crypt
if [ "$GPG_PRIVATE_KEY" != "" ]; then
    gpg-agent --daemon || true
    echo "$GPG_PRIVATE_KEY" | base64 -d > ./.private.key
    gpg --batch --import ./.private.key
    git secret reveal -f
    rm ./.private.key
else
    echo "WARN: No GPG_PRIVATE_KEY private key set, we use the host one"
fi

if [ "$ACTION" == "UPDATE" ]; then
    ${SSH} "nix-channel --update"
fi

nixos-rebuild -I \
nixos-config=./configuration.nix \
build

nixos-rebuild -I \
nixos-config=./configuration.nix \
switch

git secret hide -v -F -d
