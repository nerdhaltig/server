# nix-channel --add https://gitlab.com/stackshadow/mgtt/-/archive/develop/mgtt-develop.tar.bz2 mgtt
# nix-channel --update mgtt
{ config, pkgs }:
let
  mgttPackageSource = fetchTarball {
    url = "https://gitlab.com/nerdhaltig/mgtt/-/archive/v0.37.2/mgtt-v0.37.2.tar.bz2";
  };
  mgttPackage = (import mgttPackageSource { });

  #mgttPackage = (import <mgtt> { });
in

{
  imports = [
    mgttPackage.module
  ];

  services.mgtt = {
    enable = true;
    config = {
      level = "debug";
      json = false;

      # we serve an non tls-server ( we use traefik for TLS )
      url = "tcp://127.0.0.1:1883";

      plugins = {
        acl = {
          enable = true;
          rules = {
            stackshadow = [
              {
                direction = "w";
                route = "#";
                allow = true;
              }
              {
                direction = "r";
                route = "#";
                allow = true;
              }
            ];
            smartphone = [
              {
                direction = "w";
                route = "devices/myphone/#";
                allow = true;
              }
            ];

            # our influxdb can read all sensors
            mqttinfluxer = [
              {
                direction = "r";
                route = "sensors/#";
                allow = true;
              }
            ];

            boatsensora = [
              {
                direction = "r";
                route = "sensors/boata/#";
                allow = true;
              }
              {
                direction = "w";
                route = "sensors/boata/#";
                allow = true;
              }
            ];
          };
        };
        auth = {
          enable = true;
          users =
            {
              stackshadow = {
                groups = [ ];
                password = "JDJhJDEwJHZjRHBnT3ZrMjlMVE93RjNUMkpiNmVXNHluRDlCdnhqQVEvbjlRWklOZFNFVWdTWlBsTzd5";
              };
              smartphone = {
                password = "JDJhJDEwJDVZVXNTNGZaOS5XZDFuRlR5aVlIaWV0ejg3ell0L1piM2VXOEZWSE9MbkQ5d2J5Qjd5NjNx";
              };
              boatsensora = {
                password = "JDJhJDEwJG1NaG0uemx3Uk1OVGg1ZnNKckhRdE94cGNhREZXQlBtUEdtN0plY0NpTVFLU3QuczZmNUJx";
              };
              mqttinfluxer = {
                password = "JDJhJDEwJEdBWjFSbENTaUlLdDJTUnNvUDZOcnVvQnpFZjhpbEhEWDJDdkR2Yms4dlkwa0ozM3U2Zkxp";
              };
            };
        };
      };

      # we disable TLS
      tls = {
        cert = {
          file = "";
        };
      };

      db = "./messages.db";
    };

  };

  services.traefik = {
    enable = true;

    staticConfigOptions = {
      entryPoints = {
        mqtt = { address = ":8883"; };
      };
    };

    dynamicConfigOptions = {
      tcp = {
        routers = {
          mqtt = {
            # won't listen to entry point web
            entryPoints = [
              "mqtt"
            ];

            # because the tls of ESPHome not send the hostsni correctly, we need to listen to all on this port
            rule = "HostSNI(`*`)";
            service = "mqtt";
            # will route TLS requests (and ignore non tls requests)
            tls = {
              certresolver = "letsencrypt";
              domains = [
                {
                  main = "mqtt.nerdhaltig.de";
                }
              ];
            };

          };
        };


        services = {
          mqtt = {
            loadBalancer = {
              servers = [
                { address = "127.0.0.1:1883"; }
              ];
            };
          };

        };
      };

    };
  };

  networking.firewall.allowedTCPPorts = [ 8883 ];

}
