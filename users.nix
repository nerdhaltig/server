{ config, pkgs }:
let
  inherit (pkgs) writeShellScriptBin;

in
{

  security.sudo.extraRules = [
    # Allow execution of nixos-rebuild-script by gitlab-runner user 
    {
      users = [ "gitlab-ci" ];
      commands = [
        { command = "/run/current-system/sw/bin/nixos-rebuild"; options = [ "SETENV" "NOPASSWD" ]; }
        { command = "/run/current-system/sw/bin/nix-channel"; options = [ "SETENV" "NOPASSWD" ]; }
      ];
    }
  ];

  # a gitlab-ci-user that is allowed to rebuild-switch
  users.users.gitlab-ci = {
    shell = pkgs.zsh;
    isNormalUser = true;
    extraGroups = [ "docker" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAVPJq3PZUVFLyBriSdlPDriQCwSznuL65PK8ecqboCk ci@gitlab"
    ];
  };

}
