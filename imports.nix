{ config, pkgs, ... }: {
  imports = [
    (import ./nix.nix { inherit config pkgs; })
    (import ./swap.nix { inherit config pkgs; })
    (import ./packages.nix { inherit config pkgs; })
    (import ./users.nix { inherit config pkgs; })
    (import ./service-ssh.nix { inherit config pkgs; })
    (import ./service-gitlabrunner.nix { inherit config pkgs; })
    (import ./service-fail2ban.nix { inherit config pkgs; })
    (import ./service-traefik.nix { inherit config pkgs; })
    (import ./service-nerdhaltig.nix { inherit config pkgs; })
    (import ./service-mail.nix { inherit config pkgs; })
    (import ./service-roundcube.nix { inherit config pkgs; })
    (import ./service-mgtt.nix { inherit config pkgs; })
    (import ./service-influxdb.nix { inherit config pkgs; })
    (import ./service-telegraf.nix { inherit config pkgs; })
  ];
}
