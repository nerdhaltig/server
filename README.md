# Install nixos on hetzner-cloud

Nerdhaltig.de läuft auf Hetzner-Cloud.
Zu aller erst muss man sich einen Cloud-Server shoppen und einen SSH-Key-Erzeugen

`ssh-keygen -t ed25519 -b 4096 -f sshkey`

- Unter ISO-Images sucht an `nixos` und bindet das Image ein
- Server ausschalten
- Server einschalten
- Hetzner-Konsole öffnen

- `sudo bash`
- `parted /dev/sda -- mklabel gpt`
- `parted /dev/sda -- mkpart primary 1MiB 100%`
- `mkfs.ext4 -L nixos /dev/sda1`
- `mount /dev/disk/by-label/nixos /mnt`
- `nixos-generate-config --root /mnt`
- aktivieren von: `boot.loader.grub.device`
- ssh-aktivieren und root-pw erlauben ( vorläufig ) `services.openssh.permitRootLogin = "yes";`

