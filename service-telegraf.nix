{ config, pkgs }:
let
  secrets = import ./credentials.nix;

  telegrafPkgs = import
    (builtins.fetchGit {
      # Descriptive name to make the store path easier to identify                
      name = "telegraf-1.22.1";
      url = "https://github.com/NixOS/nixpkgs/";
      ref = "refs/heads/nixpkgs-unstable";
      rev = "bf972dc380f36a3bf83db052380e55f0eaa7dcb6";
    })
    { };

  telegrafPkg = telegrafPkgs.telegraf;

in
{

  services.telegraf = {
    enable = true;
    package = telegrafPkg;
    extraConfig = {
      inputs = {
        cpu = {
          percpu = false;
          totalcpu = true;
        };
        disk = {
          ignore_fs = [ "tmpfs" "devtmpfs" "devfs" "iso9660" "overlay" "aufs" "squashfs" ];
        };
        mqtt_consumer = [
          {
            servers = [ "tcp://127.0.0.1:1883" ];
            topics = [
              "sensors/+/sensor/+/state"
            ];
            username = "mqttinfluxer";
            password = secrets.mqtt.mqttinfluxer.password;
            data_format = "value";
            data_type = "integer";
            topic_parsing = [
              {
                topic = "sensors/boata/sensor/+/state";
                measurement = "_/_/_/measurement/_";
                tags = "_/sensor/_/_/_";
                #types = {
                #  state = "string";
                #};
              }

            ];
          }

        ];
      };

      outputs = {
        influxdb_v2 = {
          urls = [
            "http://localhost:8086"
          ];
          token = secrets.telegraf.token;
          organization = "nerdhaltig";
          bucket = "raw";
        };
      };

    };
  };

}


