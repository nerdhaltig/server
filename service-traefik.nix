{ config, pkgs }: {

  services.traefik = {

    enable = true;

    # static config
    staticConfigOptions = {
      api = { dashboard = true; };
      # providers = { docker = { exposedbydefault = false; }; };
      entryPoints = {
        http = {
          address = ":80";
          http = {
            redirections = {
              entryPoint = {
                to = "https";
                scheme = "https";
              };
            };
          };
        };
        https = { address = ":443"; };
      };

      certificatesResolvers = {
        letsencrypt = {
          acme = {
            tlsChallenge = { };
            email = "stackshadow@nerdhaltig.de";
            storage = "/secrets/traefik-acme.json";
          };
        };
      };

    };

    dynamicConfigOptions = {
      http = {
        routers = {
          traefikdashboard = {
            entrypoints = "https";
            tls = { certresolver = "letsencrypt"; };
            rule = "Host(`traefik.nerdhaltig.de`)";
            middlewares = "authadmin";
            service = "api@internal";
          };
        };

        middlewares = {
          authadmin = {
            digestauth = { usersfile = "/secrets/traefik-syncthing-pass"; };
          };
        };

      };
    };
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];

}
