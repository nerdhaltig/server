{ config, pkgs }:
let
  secrets = import ./credentials.nix;

  preBuildScript = pkgs.writeShellApplication {
    name = "preBuildScript.bash";
    runtimeInputs = [ pkgs.git pkgs.git-secret pkgs.gnupg ];
    text = ''
      trap cleanup 0

      cleanup()
      {
        git secret hide -d -F
      }
      
      gpg --version
      gpgconf --kill gpg-agent || true
      gpg-agent --daemon || true
      gpg --batch --import <(echo "$GPG_PRIVATE_KEY" | base64 -d )
      
      echo "Reveal secrets"
      git secret reveal -f
      
      nixos-rebuild -I nixos-config=./files/migrationsvm/nixos/configuration.nix build
      nixos-rebuild -I nixos-config=./files/migrationsvm/nixos/configuration.nix switch

      # ensure that nothing is in /etc/nixos/
      rm -fR /etc/nixos/*
      rm -fR /var/lib/gitlab-runner/.gnupg
    '';
  };

in
{
  services.gitlab-runner = {
    enable = true;
    #package = gitlabRunnerPackage;
    configFile = builtins.toFile "gitlab-runner.toml" ''
      concurrent = 1
      check_interval = 0

      [session_server]
        session_timeout = 1800

      [[runners]]
        name = "nerdhaltig-server"
        url = "https://gitlab.com/"
        token = "${secrets.gitlab-runner.token}"
        executor = "shell"
        limit = 4

        [runners.custom_build_dir]
        [runners.cache]
          [runners.cache.s3]
          [runners.cache.gcs]
    '';
  };

  systemd.services.gitlab-runner.serviceConfig = {
    User = "root";
    DynamicUser = pkgs.lib.mkForce false;
    PrivateTmp = pkgs.lib.mkForce false;
    ProtectSystem = pkgs.lib.mkForce false;
    ReadWritePaths = [
      "/boot" # nixos setup the bootloader, so we need access to this
      "/usr/bin" # some activation-scripts of nixos need this
      "/nix/store"
      "/nix/var/nix/profiles"
      "/run"
      "/var/lib/gitlab-runner"
    ];
  };

}
