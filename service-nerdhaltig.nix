{ config, pkgs }:
let
  nerdhaltig = (import <nerdhaltig> { });
in
{

  imports = [ nerdhaltig.module ];

  services.homepages.nerdhaltig = {
    enable = true;
    package = nerdhaltig.public;
    domain = "www.nerdhaltig.de";
  };


  # enable all services needed by homepages
  services.nginx.enable = true;
  services.traefik.enable = true;
  services.dnsmasq.enable = false;


}


